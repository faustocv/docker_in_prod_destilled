#!/bin/bash
set -e

# Install Docker
sudo curl -sSL https://get.docker.com/ | sh
sudo usermod -aG docker vagrant
curl -L https://github.com/docker/compose/releases/download/1.17.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
sudo chmod a+x /usr/local/bin/docker-compose

# Install LXC/LXD
sudo apt-get install -y lxd lxd-client
sudo usermod -aG lxd vagrant
