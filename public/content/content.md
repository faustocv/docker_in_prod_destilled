class: center, middle, main-title

# Docker in Production Distilled Workshop

By Continuous Delivery Community

---

class: center, middle, subtitle-1

# Why this workshop?

---

class: common-content

## Purposes

* Get better understarding about containers.

* How to use containers in our projects.

* Support our Continuous Delivery community.

---

class: center, middle, subtitle-1

# Topics

---

class: common-content

<!--slide # 5-->
## Topics


* [ Getting Started ](#6) -> Getting your workshop VM ready to use Docker.

* [ Module 1 ](#9) -> Getting Docker basics.

* [ Module 2 ](#50) -> Docker compose, volume management, networking and security aspects.

* [ Module 3 ](#1) -> Clustering deployment and service discovery.

* [ Module 4 ](#1) -> Monitoring and operational concerns.

* [ External Resources ](#71) -> Books, online courses, etc.

---

class: center, middle, subtitle-2

# Getting your workshop ready to use Docker
Getting Started

---

class: common-content

## Open up the slides

* Prerequisities: NodeJS and NPM.

* Install dependencies:

```bash
$ cd <SLIDES_PATH>/docker_in_prod_destilled
$ npm i
```

* Open up the slides:

```bash
$ npm run slides
```

* See slides in the following URL:

```
http://localhost:8080
```

---

class: common-content

## Getting your workshop VM ready to use Docker

* Prerequisities: Homebrew (MacOSx).

* Install dependencies (VirtualBox and Vagrant):

```bash
$ ./scripts/install_dependencies.sh
```

* Boot the VM up:

```bash
$ cd vm
$ vagrant up
```

* Check docker installation:

```bash
$ vagrant ssh
$ docker info
$ docker run --rm hello-world
```

---

class: center, middle, subtitle-3

<!--slide # 9-->
# Getting Docker Basics
Module 1

---

class: common-content

## Getting Docker Basics - Agenda

Part I:

* VM vs. Containers.

* What is a Container?

* What about Docker?

Part II:

* Working with Docker from command-line.

* Building and tagging images.

* Registries.

---

class: common-content

## VM vs. Containers

<table class="images">
  <tr>
    <td><img src="assets/vm_layers.png" alt="vm_layers" class="half_image"></td>
    <td><img src="assets/container_layers.png" alt="container_layers" class="half_image"></td>
  </tr>
</table>

---

class: common-content

## VM vs. Containers (2)

| VM                         | Containers                        |
| ---                        | ---                               |
| Larger footprints          | Isolated                          |
| Duplication of OS          | Shared OS kernel                  |
| Duplication of shared libs | Faster deployment                 |
| Requires HW virtualization | Faster restarts                   |
| Duplication of kernel      | Easier migration                  |
|                            | Run anywhere (over Docker Engine) |

---

class: common-content

## What is a Container?

.key-word[Container is a solution]

.center[.normal-word[how to get software to run reliably]]

.right[.key-word[when moved from one enviroment to another.]]

.right[.content-source[Introduction to Kubernetes using Docker by Stone River eLearning, 2016]]

---

class: common-content

## What is a Container? (2)

What can I do with a container?

* I can SSH into the container.

* I can have root access.

* I can install packages.

* I can mount filesystems.

* I have my own network interface.

* I can alter iptables rules, and the routing table.

* I can have inner containers.

---

class: common-content

## What is a Container, but technically? (3)

* Own process namespaces (pid, mnt, net, ipc, uts).

* Isolation with cgroups (memory, CPU, blkio).

* User root access

* Shared kernel with host.

---

class: center, middle, subtitle-3

# But, Is Docker the solely container technology?

---

class: common-content

## What is a Container? (4)

Some container technologies

.center[.key-word[rkt] <span>&nbsp;&nbsp;</span> .key-word[runC] <span>&nbsp;&nbsp;</span> .key-word[containerd]]

.center[.key-word[LXC/LXD] <span>&nbsp;&nbsp;</span> .normal-word[Docker] <span>&nbsp;&nbsp;</span> .key-word[OpenVZ]]

.center[.key-word[systemd-nspawn] <span>&nbsp;&nbsp;</span> .key-word[machinectl] <span>&nbsp;&nbsp;</span> ]

.center[.key-word[qemu-kvm, lkvm]]

---

class: center, middle, subtitle-3

# Exercise 1/6
Launching containers with LXC

---

class: common-content

## Launching containers with LXC

* Configure LXD. Choose default configurations.

```bash
$ sudo lxd init
```

* List all containers.

```bash
$ lxc list
```

* Start a container named "demo-container" from Alpine Linux image.

```bash
$ lxc launch images:alpine/3.6 demo-container

```

* See information about the running container.

```bash
$ lxc info demo-container
```

---

class: common-content

## Launching containers with LXC (2)

* Login into the running container.

```bash
$ lxc exec demo-container -- ash
```

* Once logged into the container, execute the following command:

```bash
~ hostname
```

* Create a file in the container. .standout-word[Note: ~ -> means execute commands inside the container.]

```bash
~ echo "hello world" > my_text.txt
~ cat my_text.txt
~ exit
```

---

class: common-content

## Launching containers with LXC (3)

* Clone the demo-container and name the new one as "cloned-demo-container"

```bash
$ lxc copy demo-container cloned-demo-container
```

* Start the cloned-demo-container and verify if all containers are running.

```bash
$ lxc start cloned-demo-container
$ lxc list
```

* Open the file my_text.txt from the cloned-demo-container.

```bash
$ lxc exec cloned-demo-container -- cat my_text.txt
```

* Destroy the cloned container.

```bash
$ lxc stop cloned-demo-container
$ lxc delete cloned-demo-container
```

---

class: center, middle, subtitle-3

# What about Docker?

---

class: common-content

## What about Docker?

.center[![Moby logo](./assets/moby_project_logo.png)]


The Docker term means:
* Docker (the company:  Docker Inc.).
* Docker (the commercial software products: Docker CE and Docker EE).
* Docker (the open source project, before April 2017).

Now, .standout-word[the open source project is called Moby.]

---

class: common-content

## What about Docker? (2)

.normal-word[Docker is a container technology]

.center[.key-word[to build, ship and run distributed applications.]]

.center[![Docker ecosystem](./assets/scaled-docker.png)]
.right[.content-source[https://www.docker.com/what-container]]

---

class: common-content

## What about Docker? (3)

Docker image as build artefact.

![Docker as artefact](./assets/docker_as_artefact.png)

---

class: common-content

## What about Docker? (4) - Docker Architecture

Main components

| Name | Explanation |
|--- |--- |
| Docker engine / Docker daemon / Docker host | the runtime to run docker containers |
| Docker client | e.g. CLI, docker-api Ruby gem. |
| Container |  the isolated environment in which an application runs |
| Image | the snapshot of a container, inert and immutable. |
| Registry | a place to storage images. |

---

class: common-content

## What about Docker? (5) - Docker Architecture

.center[
  <img src="assets/docker_architecture.svg" alt="Docker architecture" style="width: 80%;">
]

.right[
  .content-source[https://docs.docker.com/introduction/understanding-docker/]
]

---

class: center, middle, subtitle-3

# Exercise 2/6
Understading isolation using namespaces

---

class: common-content

## Understading isolation using namespaces

* Create and look inside a docker container.

```bash
$ docker run -it --rm busybox
```

* See *pid* namespace.

```bash
~ ps aux
```

* See *mnt* namespace.

```bash
~ cat /proc/mounts
```

* See *uts* namespace.

```bash
~ hostname
```

---

class: common-content

## Understading isolation using namespaces (2)

* See *ipc* namespace.

```bash
~ ipcs
```

* Exit and delete the current container.

```bash
~ exit
```

---

class: center, middle, subtitle-3

# Working with Docker from command-line
Module 1 (Part II)

---

class: center, middle, subtitle-3

# Exercise 3/6
Running your first Docker container

---

class: common-content

## Running your first Docker container

* Fetch an Alpine image from Docker Hub.

```bash
$ docker pull alpine
```

* List all images.

```bash
$ docker images
```

* Create a container based on Alpine image. List all files of the home directory.

```bash
$ docker run alpine ls -la
```

* Execute an echo message from a new container.

```bash
$ docker run alpine echo "Hello from Alpine"
```

---

class: common-content

## Running your first Docker container (2)

* Create a new container with opened shell session.

```bash
$ docker run alpine /bin/sh
$ docker run -it --name shell_container alpine /bin/sh
~ ls -la
~ uname -a
~ exit

```

* Show all currently running containers.

```bash
$ docker ps
$ docker ps -a
```

* Start the container again.

```bash
$ docker start shell_container
```

---

class: common-content

## Running your first Docker container (3)

* Attach your terminal with the running container.

```bash
$ docker attach shell_container
~ hostname
~ exit

```

* Remove *shell_container* container.

```bash
$ docker rm shell_container
```

---

class: center, middle, subtitle-3

# Exercise 4/6
Run a static website in a container

---

class: common-content

## Run a static website in a container

* Launch a container in **detached** mode.

```bash
$ docker run --name static-site -e AUTHOR="Your Name" -d -p 80:80 dockersamples/static-site
```

* Open a browser and test your application.

```
http://localhost:8082
```

* Stop and remove the container *static-site*

```bash
$ docker stop static-site
$ docker rm static-site
```

---

class: center, middle, subtitle-3

# Building and tagging images

---

class: common-content

## A bit more about Images

* *Base images* -> have no parent images. E.g. OS images.

```bash
$ docker pull ubuntu
```

* *Child images* -> are build on parent images and add additional funcionality. E.g. GOCD images.

```bash
docker pull gocd/gocd-server
```

* *Official images* -> images managed by Docker Inc. These are not prefixed by organization or user name. E.g. PostgreSQL image.

```bash
$ docker pull postgres
```

* *User images* -> E.g. My custom PostgreSQL image.

```bash
$ docker pull myuser/mypostgres
```

---

class: center, middle, subtitle-3

# Exercise 5/6
Create your first image

---

class: common-content

## Create your first image

* Address to the flask app directory.

```bash
cd /vagrant/exercises/flask_app
```

* Create and fill up the Dockerfile.

```Dockerfile
FROM alpine:3.5

RUN apk add --update py2-pip

RUN pip install --upgrade pip

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

COPY app.py /usr/src/app/
COPY templates/index.html /usr/src/app/templates/

EXPOSE 5000

CMD ["python", "/usr/src/app/app.py"]
```
---

class: common-content

## Create your first image (2)

* Build your first image

```bash
$ docker build -t myuser/myfirstapp .
```

* Run your image

```bash
$ docker run -p 80:5000 -d --name myfirstapp myuser/myfirstapp
```

* Open a browser and test your application.

```
http://localhost:8082
```

* Stop and remove the container *myfirstapp*.

```bash
$ docker stop myfirstapp
$ docker rm myfirstapp
```

---

class: center, middle, subtitle-3

# Exercise 6/6
Tagging and pushing your image

---

class: common-content

## Tagging and pushing your image

* Create an account in *Docker Hub*. Retrieve your username.

* Tag your image with your Docker Hub username

```bash
$ docker tag myuser/myfirstapp [docker hub username]/myfirstapp
```
* Log in at Docker Hub registry.

```bash
$ docker login
```

* Upload your image towards Docker Hub registry.

```bash
$ docker push [docker hub username]/myfirstapp
```

---

class: common-content

## Tagging and pushing your image (2)

* Run your tagged image by creating a new container.

```bash
$ docker run -p 80:5000 -d --name newapp [docker hub username]/myfirstapp
```

* Open a browser and test your application.

```
http://localhost:8082
```

* Stop and remove the container *newapp*.

```bash
$ docker stop newapp
$ docker rm newapp
```

---

class: center, middle, subtitle-3

# Wrap up
Module 1

---

class: common-content

## Wrap up - Module 1

What have we learned?

* What a Container is.

* Basic info about Docker.

* Diferences between VM and Containers.

* Command for managing Docker images and containers.

* A bit about Registries. More registries: Artifactory, Nexus, Docker Container Registry.

---

class: center, middle, subtitle-3

# Homework 1

---

class: common-content

## Tasks

1. Create a LXC container based on "ubuntu-daily:16.04". The container must be named as "docker-holder".
2. Install Docker inside "docker-holder" container.
3. Create a Dockerfile file about an static website. The image must be based on NGINX official image. For this task download any static website template from [HTML5UP](https://html5up.net/), and serve it at NGINX server's working directoy.
4. Create an image. Tag it as "myuser/static-website".
5. Create a Docker container called "static_website_container". It must be based on "myuser/static-website" image. Expose the service at 80 port in all network interfaces of "docker-holder" container.
6. Be sure to map ports inside vagrant from 80 (docker-holder container) to 80 (vagrant box) port.
7. See the results.

Clues: See this [blog](https://stgraber.org/2016/04/13/lxd-2-0-docker-in-lxd-712/)

---

class: center, middle, subtitle-1

# Docker Compose, volume management, networking and security aspects
Module 2

<!--slide # 50-->

---

class: common-content

## Docker Compose, volume management, networking and security aspects

Agenda:

* Homework #1 review.

* Volume management.

* Docker Compose.

* Networking basics.

* Security aspects to be aware of.

---

class: center, middle, subtitle-1

# Homework #1 review

---

class: center, middle, subtitle-1

# Volume management

---

class: common-content

## What is a Volume?

.key-word[Volume is a mechanism]

.center[.normal-word[for persisting data]]

.right[.key-word[by and used by Docker containers]]

.right[.content-source[https://docs.docker.com]]

---

class: center, middle, subtitle-1

# Exercise 1/5
Creating an independent volume

---

class: common-content

## Creating an independent volume

* Create a volume named `DataVolume1`

```bash
$ docker volume create --name DataVolume1
```

* Create a new container from an `Ubuntu` image with the `DataVolume1` volume attached to it.

```bash
$ docker run -ti --rm -v DataVolume1:/datavolume1 ubuntu
```

* Write some data to the volume. Then exit from the container.

```bash
~ echo "Example1" > /datavolume1/Example1.txt
~ exit
```

* Verify if the volume `DataVolume1` exists in your system

```bash
$ docker volume inspect DataVolume1
```

---

class: common-content

## Creating an independent volume (2)

* Let's start a new container and attach `DataVolume1`

```bash
$ docker run -ti --rm -v DataVolume1:/datavolume1 ubuntu
```

* Verify if your previous saved data persists

```bash
~ cat /datavolume1/Example1.txt
~ exit
```
---

class: center, middle, subtitle-1

# Exercise 2/5
Creating a volume that persists when the container is removed

---

class: common-content

## Creating a volume that persists when the container is removed

* Create a container called `Container2` with a volume `DataVolume2`.

```bash
$ docker run -ti --name=Container2 -v DataVolume2:/datavolume2 ubuntu
```

* Write some data inside the volume

```bash
~ echo "Example2" > /datavolume2/Example2.txt
~ cat /datavolume2/Example2.txt
~ exit
```

* Restart the container. The volume will mount automatically.

```bash
$ docker start -ai Container2
```

* Verify if data is still in place.

```bash
~ cat /datavolume2/Example2.txt
~ exit
```
---

class: common-content

## Creating a volume that persists when the container is removed (2)

* Let's exit and clean up the created volume.

```bash
$ docker volume rm DataVolume2
```

* Solve the last issue by deleting the container first.

```bash
$ docker rm Container2
```

* Verify if deleting `Container2` won't affect volume `DataVolume2`.

```bash
$ docker volume ls
```

* Remove container `Container2`.

```bash
$ docker volume rm DataVolume2
```
---

class: center, middle, subtitle-1

# Exercise 3/5
Creating a volume from an existing directory with data

---

class: common-content

## Creating a volume from an existing directory with data

* Create a container and add the volume at `/var`, a directory which contains data in the base image.

```bash
$ docker run -ti --rm -v DataVolume3:/var ubuntu
~ exit
```

* List the data contained in `DataVolume3` by creating of a new container.

```bash
$ docker run --rm -v DataVolume3:/datavolume3 ubuntu ls datavolume3
```

The directory `/datavolume3` should have a copy of the base image's `/var` directory content.

---

class: center, middle, subtitle-1

# Exercise 4/5
Sharing data between multiple Docker containers

---

class: common-content

## Sharing data between multiple Docker containers

* Create a new container named `Container4` with a volume attached.

```bash
$ docker run -ti --name=Container4 -v DataVolume4:/datavolume4 ubuntu
```

* Create some data in /`datavolume4`. Then, exit.

```bash
~ echo "This is a file shared between containers" > /datavolume4/Example4.txt
~ exit
```

* Create a new container named `Container5` and mount the `Container4`'s volumes

```bash
$ docker run -ti --name=Container5 --volumes-from Container4 ubuntu
```

* View the directory /`datavolume4` content.

```bash
~ cat /datavolume4/Example4.txt
```

---

class: common-content

## Sharing data between multiple Docker containers (2)

* Append new content in the file `Example4.txt` from the `Container5`. Then, exit.

```bash
~ echo "Both containers can write to DataVolume4" >> /datavolume4/Example4.txt
~ exit
```

* View changes made in `Container5` from `Container4`. Then, exit.

```bash
$ docker start -ai Container4
~ cat /datavolume4/Example4.txt
~ exit
```

* Create a new container named `Container6` and mount `DataVolume4` volume as read only.

```bash
$ docker run -ti --name=Container6 --volumes-from Container4:ro ubuntu
```

---

class: common-content

## Sharing data between multiple Docker containers (3)

* Try to remove `Example4.txt` file from `Container6`. Then, exit.

```bash
~ rm /datavolume4/Example4.txt
~ exit
```

* Let's clean up containers and volume.

```bash
$ docker rm Container4 Container5 Container6
$ docker volume rm DataVolume4
```
---

class: center, middle, subtitle-1

# Exercise 5/5
Sharing data between host and container

---

class: common-content

## Sharing data between host and container

* Let's create some data from host `/datavolume5` directory.

```bash
$ mkdir /datavolume5
$ echo "This is a file created from host" > /datavolume5/Example5.txt
```

* Create a new container named `Container7` and mount `/datavolume5` host directory.

```bash
$ docker run -ti --rm --name=Container7 -v /datavolume5:/datavolume5 ubuntu
```

* View the directory /`datavolume5` content.

```bash
~ cat /datavolume5/Example5.txt
```

* Append new content in the file `Example5.txt` from the `Container7`. Then, exit.

```bash
~ echo "Adding new content from Container7" >> /datavolume5/Example5.txt
~ exit
```

---

class: common-content

## Sharing data between host and container (2)

* View the `/datavolume5/Example5.txt` file content from host.

```bash
$ cat /datavolume5/Example5.txt
```

---

class: common-content

## Volume management

Special considerations about volumes:

* Volume content is not included in the docker image.

* Any changes in volume content (via RUN) is not part of image.

* Can be shared and reused among containers.

* Persists even if the container itself is deleted.

* Sharing/synchronizing host directories with containers' ones.

* In the case of sharing a volume between multiple containers, Docker doesn't support file locking. Available option read-only access.

---

class: center, middle, subtitle-3

# External Resources

<!--slide # 71-->

---

class: common-content

## External Resources

Books:

* [Docker para desenvolvedores](https://leanpub.com/dockerparadesenvolvedores), Rafael Gomes.

Posts:

* [How To Share Data between Docker Containers](https://www.digitalocean.com/community/tutorials/how-to-share-data-between-docker-containers), Melissa Anderson.

Online Courses:

* [Linux Academy](https://linuxacademy.com/), *Docker Deep Dive* course.


